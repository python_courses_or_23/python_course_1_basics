def who_is_missing(file_name):
	""" This function finds the missing number in the file's data going from 0
		:param file_name: the file's path
		:type file_name: str
		:rtype: None
	"""
	file_read = open (file_name, 'r')
	file_write = open ('found.txt', 'w')
	
	numbers = file_read.read()
	lst = numbers.split(',')
	lst.sort()
	for i in range(len(lst)):
		if int(lst[i]) != (i+1) :
			missing = str(i+1)
			file_write.write(missing)
			return (i+1)
	print ("There seems not to be a missing number.... that's weird")		
	file_write.write("There seems not to be a missing number.... that's weird")
	file_read.close()
	file_write.close()
	return None

print(who_is_missing("find_me.txt"))
