def are_files_equal(file1, file2):
	""" This function checks if the two given files contain the same data
		:param file1: the first file's path
		:param file2: the second file's path
		:type file1: str
		:type file2: str
		:return: whether the two given files contain the same data
		:rtype: bool
	"""
	
	f1 = open (file1, 'r')
	f2 = open (file2, 'r')
	equ = f1.read() == f2.read()
	f1.close()
	f2.close()
	return equ