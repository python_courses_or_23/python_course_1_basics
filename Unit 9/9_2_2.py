def copy_file_content(source, destination):
	""" This function copies the text inside the first file into the second file
		:param source: the first file's path
		:param destination: the second file's path
		:type source: str
		:type destination: str
		:rtype: None
	"""
	file_read = open (source, 'r')
	file_write = open (destination, 'w')
	str = file_read.read()
	file_write.write(str)
	file_read.close()
	file_write.close()
	
copy_file_content ('source.txt', 'destination.txt')