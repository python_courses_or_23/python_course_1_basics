def my_mp4_playlist(file_path, new_song):
	""" This function read songs attributes from a text file and
		switches the third song's name to the new name
		:param file_path: the file's path
		:type file_path: str
		:param new_song: the new song's name
		:type new_song: str
		:rtype: None
	"""
	f = open(file_path, 'r')
	songs_lst = f.read().split('\n') # list of songs
	# changing the list
	if len(songs_lst) < 3 :
		# len(songs_lst) = number on \n +1 = number of rows 
		f.close()
		f = open(file_path, 'w')
		f.write((3-len(songs_lst)) * '\n')
		f.write(new_song)
		f.close()
	else :		
		songs3_atrributes = songs_lst[2].split(';')
		songs3_atrributes[0] = new_song
		songs_lst[2] = ';'.join(songs3_atrributes)
		
		# writing the list again to the file
		f.close()
		f = open(file_path, 'w')
		str = '\n'.join(songs_lst)
		print(str)
		f.write(str)
		f.close()

my_mp4_playlist	('songs.txt', 'Python Love Story')