def my_mp3_playlist(file_path):
	""" This function read songs from a text file and
		returns a tuple consisting of
		(max length song, the number of songs written inseide the file, the artist whose songs appear the most)
		:param file_path: the file's path
		:type file_path: str
		:return: the spoken tuple
		:rtype: a 3-attribute-tuple
	"""
	with open(file_path, 'r') as f :
		songs_lst = f.read().split('\n')
		max_len = -1
		max_song = ''
		artists_dict = {}

		for song in songs_lst :
			song_attributes = song.split(';')
			# getting the length of the song
			len_min_and_second = song_attributes[2].split(':') 
			seconds = int(len_min_and_second[0]) * 60 + int(len_min_and_second[1])
			if seconds > max_len :
				max_len = seconds
				max_song = song_attributes [0]
			artist = song_attributes[1]
			if artist in artists_dict :
				artists_dict[artist] += 1
			else :
				artists_dict[artist] = 1
		
		max_art = max(artists_dict.values())
		for key in artists_dict.keys():
			if artists_dict[key] == max_art :
				return (max_song, len(songs_lst), key)		

print(my_mp3_playlist(r"songs.txt"))