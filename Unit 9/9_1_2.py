def tasks (path, action) :
	""" This function perform 3 various tasks on a file given the file's path and the whished action to perform
		:param path: the file's path
		:param action: the action to perform
		:type path: str
		:type action: str
		:rtype: None
	"""
	read_file = open (path, 'r')
	words_list = list()
	if action == "sort" :
		for line in read_file :
			lst = line.split(' ')
			if lst[-1].endswith('\n') :
				lst [-1] = lst[-1][0:-1]
			for word in lst :
				if word not in words_list :
					words_list.append(word)
		words_list.sort()
		print(words_list)
	
	elif action == 'rev' :
		for line in read_file :
			lst = line.split(' ')
			if lst[-1].endswith('\n') :
				lst [-1] = lst[-1][0:-1]
			# now list contains a list of the words
			str = ' '.join(lst)
			print (str[-1::-1])
			
	elif action == "last" :
		n = int(input('Please enter a number N : '))
		indx = 0
		for line in read_file :
			if indx < n :
				indx += 1
				continue
			else :	
				print (line)
	read_file.close()	


def main () :
	path = input ('Please provide the path to the text file : ')
	action = input ('Please provide the action to perform : ')
	tasks (path, action)
	
if __name__ == "__main__" :
	main ()