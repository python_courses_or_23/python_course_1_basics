""" This game was developed by Ofek Rotem on the date 22 / 04 / 2023
	this game is the famous hangman game developed in python language
"""   
# --------------------------------------------------------------------------
MAX_TRIES = 6 # this number represents the number of fails the player can make i.e. the number of phases of the hanged man
HANGMAN_ASCII_ART = """\nWelcome to the hangman game developed my Ofek Rotem
  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                      __/ |                      
                     |___/
You will have a total of """ + str(MAX_TRIES) + " guesses to reveal the hidden word, else, you'll lose"
# this is the hangman title appering in the start of the game

HANGMAN_PHOTOS  = {0:
"""
    x-------x
""", 1:
"""
    x-------x
    |
    |
    |
    |
    |
""", 2:
"""
    x-------x
    |       |
    |       0
    |
    |
    |
""", 3:
"""
    x-------x
    |       |
    |       0
    |       |
    |
    |
""", 4:
"""
    x-------x
    |       |
    |       0
    |      /|\\
    |
    |
""", 5:
"""
    x-------x
    |       |
    |       0
    |      /|\\
    |       |
    |
""", 6: 
"""
    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |
"""} # the 
# these are the 7 phases' pictures of the hangman
# --------------------------------------------------------------------------
def main () :
	print (HANGMAN_ASCII_ART)
	path = input('Please enter the full path to the hidden words : ')
	index_hidden_word = int(input('Please enter some index to use for hidding the word : '))
	tpl_ret = choose_word(path, index_hidden_word)
	if tpl_ret != None : # None was not returned from the function i.e. file is ok
		num_tries = 0
		print_hangman(num_tries)
		hidden_word_underscores(tpl_ret[1])
		old_letters = list()
		
		while num_tries < 6 :
			char = input ('Please enter your next character to guess : ')
			while not try_update_letter_guessed (char, old_letters) : # == FALSE
				char = input ('Please enter your next character to guess : ')
			print(show_hidden_word (tpl_ret[1], old_letters))
			if char not in tpl_ret[1] : # failed attempt
				print ('	):')
				num_tries += 1
				print_hangman(num_tries)

			if check_win(tpl_ret[1], old_letters) :
				print('Congratulations ! you won ! the hangman gets to go home !')
				return
		print('Oops... the poor hangman... and even worse, you have lost !')	
		
# -------------------------------HIDING A WORD ------------------------
def choose_word(file_path, index):
	""" This fucntion returns a tuple containing the number of unique words in the file given,
		as well as the chosen word from the given index 
		Its perpose is to choose a word to hide
		
		:param file_path: the full file for the hidden-words text file
		:param index: the index of the hidden word in the words list
		:type file_path: string
		:type index: int
		:return: a tuple containing the quantity of unique words and the hidden word itself
		:rtype: tuple
	"""
	try :
		with open (file_path, 'r') as f :
			dict_hidden_words = {}
			lst = f.read().split(' ')
			for word in lst :
				if word in dict_hidden_words :
					dict_hidden_words[word] += 1
				else :
					dict_hidden_words[word] = 1
	except (IOError, OSError) as e :
		print('The file is not found, wrong file name or path')
		return
	return (len(dict_hidden_words.keys()),lst[(index-1)%len(lst)] )
	
def hidden_word_underscores(hidden_word) :
	""" This procedure prints an appropriate amount of unserscores to represent the hidden word, 
		given to it.
		:param hidden_word: the hidden word
		:type hidden_word: string
		:rtype: None
	"""
	print ('    ' ,('_ ' * len(hidden_word)))

def show_hidden_word(secret_word, old_letters_guessed):
	res_list = ['_'] * len(secret_word)
	""" creating a string cimposed of the right letters and under lines for the un-guessed ones
		:param secret_word: the hidden word
		:param old_letters_guessed: the allready-guessed letters
		:type secret_word: string
		:type old_letters_guessed: list
		:return: a string representing the current state of the game's guesses 
				 i.e a string composed of '_' and of words to match the current guesees and completion of the word
		:rtype: string		 
	"""
	
	for char in old_letters_guessed :
		if char in secret_word : # the char exists in the hidden word
			unravel_indexes (secret_word, res_list, char)
	return '    ' + ' '.join(res_list)	

def unravel_indexes (str, lst, char) :
	""" This procedure fills a given list lst representing some state of uncovering the word
		with a guessed letter char, given the hidden word str
		:param str: the hidden word 
		:param lst: the list to fill 
		:param char: the guessed char
		:type str: string
		:type lst: list
		:type char: string
		:rtype: None
	"""
	for i in range (len(str)) :
		if str[i] == char :
			lst [i] = char

# ------------------------------ GUESSING A LETTER --------------------

def check_valid_input (letter_guessed, old_letters_guessed) :
	""" This fucntion returns whether the current guessed letter is a VALID guess
		meaning it fills three different critiria :
		a) it is a one char guess 
		b) it is an english letter
		c) it hasnt been guessed yet
		it does so by examining all three cased and priting an error type (1,2 or 3) accordingly
		:param letter_guessed: the guessed letter 
		:param old_letters_guessed: a list containing all previoly guessed letters, to be printed in case of an error
		:type letter_guessed: string
		:type old_letters_guessed: list
		:return: a boolean value representing whether the guess is a valid one
		:rtype: boolean 
	"""
	
	if len(letter_guessed) > 1 and not letter_guessed.isalpha() : #e3
		print("E3")
		return False
	elif len(letter_guessed) > 1: #e2
		print("E1")
		return False
	elif not letter_guessed.isalpha() : #e1
		print("E2")
		return False
	#valid, need to check that the guessed letter is not in the allready-guessed letetrs
	return letter_guessed not in old_letters_guessed
			
def try_update_letter_guessed(letter_guessed, old_letters_guessed):
	""" This fucntion is in charge of updating the list of old guessed letters with the new letter that has been guessed
		it checks that the letter is indeed LEGAL (valid) using the fucntion mentioned previous, 
		if that letter is valid, it adds it to the 'old_letters_guessed' list, 
		otherwise, it print an X followed by the allready-guessed letters
		:param letter_guessed: the new guessed letter
		:param old_letters_guessed: the list of allready-guessed letters
		:type letter_guessed: string
		:type old_letters_guessed: list
		:return: whether the guess is valid
		:rtype: boolean
	"""
	if check_valid_input (letter_guessed.lower(), old_letters_guessed):
		old_letters_guessed.append(letter_guessed.lower())
		return True
	print('X')
	print_guessed_letters(old_letters_guessed)
	return False

def print_guessed_letters(old_letters_guessed):
	""" This procedure is used for printing the allready-guessed letters to the screen, sorted alphabetically
		:param old_letters_guessed: the list of allready-guessed letters
		:type old_letters_guessed: list
		:rtype: None
	"""
	sorted_list = sorted (old_letters_guessed)
	print (' -> '.join(sorted_list))

# ------------------------------ CHECKING FOR A WIN --------------------
def check_win(secret_word, old_letters_guessed):
	""" This fucntion checks if the current guessing state is a win
		it does so by comparing the hidden word to the allready-guessed letters list
		:param secret_word: the hidden word
		:param old_letters_guessed: the list of allready-guessed letters
		:return: whether the player has won
		:rtype: bool
	"""
	for char in secret_word :
		if char not in old_letters_guessed :
			return False
	return True		

# ------------------------------ PRINTING THE HANGMAN ------------------
def print_hangman(num_of_tries):
	""" This procedure prints the current state of the hangman using the number of tries of the player
		:param num_of_tries: the number of tries of the player
		:rtype: None
	"""
	global HANGMAN_PHOTOS
	print(HANGMAN_PHOTOS[num_of_tries])
	
if __name__ == "__main__" :
	main()
