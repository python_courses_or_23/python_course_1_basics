def extend_list_x (list_x, list_y):
	""" This function recieves two lists x and y and inserts y into the start of list xrange
		:param list_x: list x
		:param list_y: list y
		:type list_x: list
		:type list_y list
		:return: the complete list x 
		:rtype: list
	"""
	list_x[0:0] = list_y
	return list_x
	
x = [4, 5, 6]
y = [1, 2, 3]
print(extend_list_x(x,y))