def longest (my_list):
	""" This function returns the longest element in a given list
		:param my_list: the list
		:type my_list: list
		:return: the maximum-length element
		:rtype: string (in that case)
	"""
	return max(my_list, key = len)
	
list1 = ["111", "234", "2000", "goru", "birthday", "09"]
print(longest(list1))