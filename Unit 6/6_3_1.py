def are_lists_equal (list1, list2) :
	""" This function returns wheter two lists are equal i.e. they contain the same elements, 
		perhaps in a different order
		:param list1: first list
		:param list2: second list
		:type list1: list
		:type list2: list
		:return: wheter the two lists are equal
		:rtype: bool
	"""
	return sorted(list1) == sorted(list2)
	
list1 = [0.6, 1, 2, 3]	
list2 = [3, 2, 0.6, 1]
list3 = [9, 0, 5, 10.5]
print(are_lists_equal(list1, list2))
print(are_lists_equal(list1, list3))