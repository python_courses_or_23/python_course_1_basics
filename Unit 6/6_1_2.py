def shift_left (my_list) :
  """ This function recives a list of a length of a 3 and changes it so that
      every element moves one index to it's left
	  :param my_list: the original list to perform a shift left on
	  :type my_list: list
	  :return: the shifted-left list
	  :rtype: list
  """
	my_list[0], my_list[1], my_list[2] = my_list[1], my_list[2], my_list[0]
	return my_list

def main () :
	print(shift_left([0, 1, 2]))
	print(shift_left(['monkey', 2.0, 1]))

if __name__ == "__main__" :
	main ()		