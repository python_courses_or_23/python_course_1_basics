def format_list (my_list):
	""" This function returns a string representing the list of chemical elements, with a
		 comma seperating them and a 'and' before the last element
		 :param my_list: list of elements
		 :type my_list: list
		 :return: the string representing the list
		 :rtype: str
	"""
	new_list =  my_list[::2] + [ "and " + str(my_list[-1])]
	return ', '.join (new_list)
	
my_list = ["hydrogen", "helium", "lithium", "beryllium", "boron", "magnesium"]
print(format_list(my_list))