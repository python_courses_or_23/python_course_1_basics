def filter_teens (a = 13, b = 13, c = 13) :
	""" This function recieves three ages and returns their sum according to the conditions
		specified in the exercise
		:param a: first age
		:param b: secong age
		:param c: third age
		:type a: int
		:type b: int
		:type c: int
		:return: the fixed ages sum
		:rtype: int
	"""
	a = fix_age(a) 
	b = fix_age(b) 
	c = fix_age(c)
	return a + b + c
	
def fix_age (age) :
	""" This fucntion 'fixes' an age i.e. if the age is between 13 and 19, and is not 15 or 16, then items
		sets it to 0, the age stays the same otherwise
		:param age: the age
		:type age: int
		:return: the fixed age
		:rtype: int
	"""
	if age >= 13 and age <= 19 and age != 15 and age != 16 :
		 return 0
	else :
		return age


print(filter_teens())
print(filter_teens(1, 2, 3))
print(filter_teens(2, 13, 1))
print(filter_teens(2, 1, 15))