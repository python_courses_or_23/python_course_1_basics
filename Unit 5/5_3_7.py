def chocolate_maker (small, big, x) :
	""" This function returns whether you could assemable an x length for a 'small' number of 
		small bricks lengthed 1 and a 'big' number of big bricks lengthed 5
		:param small: the number of small bricks
		:param big: the number of big bricks
		:param x: the length of the line
		:type small: int
		:type big: int
		:type x: int
		:return: whether you could assemable an x length for a 'small' number of 
				small bricks lengthed 1 and a 'big' number of big bricks lengthed 5
		:rtype: bool
	"""
	if x // 5 <= big and x % 5 <= small : # enough by one of the kinds OR full combiantion
		return True
	return x // 5 > big and (x - big*5) <= small

# x % 5 # שארית החלוקה ב 5, קרי מספר האחדות
# x / 5 # מנה, מספר שלם של 5
