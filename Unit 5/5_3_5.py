def distance (num1, num2, num3) :
	""" This function returns true if the dist between num2/num3 to num1 is 1 
		AND
		the distance between num2 to num1 and num3 bigger or equal to 2
		OR 
		the distance between num3 to num2 and num1 bigger or equal to 2
		:param num1: first number
		:param num2: second number
		:param num3: third number
		:type num1: int
		:type num2: int
		:type num3: int
		:return: whether the condition is met
		:rtype: bool
	"""
	return ((abs(num2-num1) == 1 or abs(num3-num1) ==1) and 
		((abs(num2-num1) >=2 and abs(num2-num3) >=2 ) or
		(abs(num3-num1) >=2 and abs(num3-num2) >=2 )))

print(distance(1, 2, 10))