def seven_boom (end_number):
	""" This function returns a list of all numbers in a seven boom game, given the ending number
		:param end_number: the last number to check for
		:type end_number: int
		:return: a list of all numbers from 0 until end_number, with boom on each number that has or is a multiplication of 7
		:rtype: list
	"""
	res_list = list()
	for i in range(end_number +1) :
		if i % 7 ==0 or ('7' in str(i)) : # the number is a 7 mul or contains the number 7 in it
			res_list.append('BOOM')
		else :
			res_list.append(i)
	return res_list		
			
print(seven_boom(17))