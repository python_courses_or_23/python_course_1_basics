def sequence_del (my_str):
	""" This function extracts the char that a in a row in a given string,
		for example, for the string ppyyyyythhhhhooonnnnn the function shall return
		the list consisting of [p,t,y,h,o,n]
		:param my_str: the given string
		:type my_str: str
		:return: the list representing the string -  with one letter from each sequence 
		:rtype: list
	"""
	unique = list()
	current_char = ''
	for i in range(len(my_str)) : # iterating over all chars in the string
		if my_str[i] == current_char :
			continue
		elif i == 0 or  my_str[i] != current_char:
			unique.append(my_str[i])
			current_char = my_str[i]

	return ''.join(unique)

print(sequence_del(""))
print(sequence_del("SSSSsssshhhh"))
print(sequence_del("Heeyyy   yyouuuu!!!"))