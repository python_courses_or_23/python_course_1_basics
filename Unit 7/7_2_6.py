groceries_list = list() # the list of items, created from a string from the user

def input_groceries_list () :
	""" This procedure gets an input of the string of some groceries list
		and perform various tasks on that list given a task number inputted as well
		:rtyep: None
	"""
	global groceries_list
	groceries_list = input('Enter your groceries list in the accroding structure : ').split(',')

def tasks (num_of_task):
	global groceries_list
	if num_of_task == 1: # printing the groceries list
		print(groceries_list)
	elif num_of_task == 2: # printing the number of item in the list
		x = len(groceries_list)
		print(x)
	elif num_of_task == 3: # printing whether the given item appears in the groceries list
		grocery = input('Please enter a specific item to check for : ')
		if grocery in groceries_list:
			print ('The specified item DOES appear in the list provided !')
		else :
			print ('The specified item DOES NOT appear in the list provided.')
	elif num_of_task == 4: # printing the quantity of a given item in the groceries list
		grocery = input('Please enter a specific item to check for : ')
		print ('The wished item appears', groceries_list.count(grocery), 'Times int the list.')
	elif num_of_task == 5: # removing an inputted item from the list 
		grocery = input('Please enter a specific item to check for : ')
		groceries_list.remove(grocery)
		print ('The item has been succsesfully removed from the groceries list !')
	elif num_of_task == 6: # adding an item to the groceries list
		grocery = input('Please enter a new item to add to the list : ')
		groceries_list.append(grocery)
	elif num_of_task == 7: # printing all ilegal items in the list
		for gro in groceries_list :
			if not gro.isalpha() or len(gro) < 3 :
				print (gro)
	elif num_of_task == 8: # removing duplicates
		for gro in groceries_list :
			if groceries_list.count(gro) > 1 :
				for i in range (groceries_list.count(gro) -1) :
					groceries_list.remove(gro)
	print('\n')
	return None	# exiting 
		

def main ():
	input_groceries_list()
	
	while True :
		number = int(input('Please enter a number between 1 and 9 representing the task you would like to be perform : '))
		if number != 9 :
			tasks(number)
		else :
			break
	
	
if __name__ == "__main__" :
	main()