def square_numbers(start, stop):
	""" This function returns a squared-numbers list from a starting number and an ending one
		:param start: the starting number
		:param stop: the end number
		:type start: int
		:type stop: int
		:return: the list of squared numbers
		:rtype: list
	"""
	square_list = list()
	while (start <= stop) :
		square_list.append(start ** 2)
		start += 1
	return square_list

print(square_numbers(-3, 3))