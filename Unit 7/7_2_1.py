def is_greater (my_list, n):
	""" This function returns a list of all numbers greater than a given number, inside a given list
		:param my_list: the list of original numbers
		:param n: the number to check with
		:type my_list: list
		:type n: int
		:return: the list of all numbers in the orig list greater than n
		:rtype: list
	"""
	res_list = list()
	for number in my_list :
		if (number > n) :
			res_list.append(number)
	return res_list
	
result = is_greater([1, 30, 25, 60, 27, 28], 28)
print(result)