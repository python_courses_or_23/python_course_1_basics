def arrow (my_char, max_length) :
	""" This procedure prints an arrow made of the character 'my_char' given, the length of the arrow in given to the procedure as well
		:param my_char: the character to print
		:param max_length: the maximum length of the created arrow
		:type my_char: str
		:type max_length: int
	""" 
	for i in range (max_length) :
		print (my_char * (i +1))
	for i in range (max_length) :
		print (my_char * (max_length - (i +1)))
		
def arrow_rec (my_char, max_length, index) :
	""" This function dows the same thing like the previous one, nut recursivly
	"""
	if index == max_length -1 :
		print (my_char * (max_length))
	else :
		print (my_char * (index +1))
		arrow_rec(my_char,max_length, index +1)
		print (my_char * (index +1))

		
arrow('*', 5)		
arrow_rec('*', 5, 0)	