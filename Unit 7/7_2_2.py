def numbers_letters_count(my_str):
	""" This function returns a list containing the quantity of number, and of letter in a given string
		:param my_str: the string
		:type my_str: string
		:return: a list containing the quantity of number, and of letter in a given string
		:rtype: list
	"""
	res_list = list()
	count_digits = 0
	for character in my_str :
		if character.isdigit() :
			count_digits += 1
	
	res_list.append(count_digits) 
	res_list.append(len(my_str) - count_digits) 
	return res_list
	
print(numbers_letters_count("Python 3.6.3"))