def count_chars(my_str):
	""" This function creates a dict filled with the quantity of each letter in a given string, not including spaces
		:param my_str: the string
		:type my_str: str
		:return: the dict of quantities
		:rtype: dict
	"""
	dict = {}
	for char in my_str :
		if char == ' ' :
			continue
		if char in dict : # the key exists in the dict
			dict[char] += 1
		else : # the key does not exist
			dict[char] = 1
	return dict
	
magic_str = "abra cadabra"	
print(count_chars(magic_str))