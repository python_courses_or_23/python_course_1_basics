def inverse_dict(my_dict):
	""" This function creats a dict made of the key and vals of the orig given dict, but reversed, meaning that the
		key become the values, and vice versa
		:param my_dict: the original dict
		:type my_dict: dict
		:return: the reversed dict
		:rtype: dict
	"""
	res_dict = {}
	for item in my_dict.items() :
		# (key, value)
		if item[1] in res_dict : # the key exists
			res_dict[item[1]].append(item[0])
		else :
			res_dict[item[1]] = [item[0]]
	return res_dict

course_dict = {'I': 3, 'love': 3, 'self.py!': 2}
print(inverse_dict(course_dict))