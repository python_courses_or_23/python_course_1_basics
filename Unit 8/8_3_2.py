my_dict = {"first_name" : "Mariah",
"last_name" : "Carey",
"birth_date" : "27.03.1970",
"hobbies" : ["sing", "Compose", "Act"]}

def main () :
	task = int(input('Please enter a number of a task you wish to perform : '))
	tasks(task)
	
def tasks (number) :
	""" This function performs a task on the global dictonary given a task number 'number'
		:param number: the task's number 
		:type number: int
		:rtype: None
	"""
	global my_dict
	if number == 1 :
		print (my_dict["last_name"])
	elif number == 2:
		print (my_dict["birth_date"][3:5])
	elif number == 3:
		print (len(my_dict["hobbies"]))
	elif number == 4:
		print (my_dict["hobbies"][-1])
	elif number == 5:
		my_dict["hobbies"].append("Cooking")
	elif number == 6:
		birth_date = my_dict["birth_date"]
		birth_date = (birth_date[:2], birth_date[3:5], birth_date[6:])
		print(birth_date)
	else :
		my_dict ["age"] = 54
		print (my_dict ["age"])
	
if __name__ == "__main__" :
	main()