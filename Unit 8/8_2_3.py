def mult_tuple(tuple1, tuple2):
	""" This function returns a tuple representing the multiplication-tuple of its two given tuples
		i.e. a tuple conatining every arrangemnt of every two element between both tuples
		:param tuple1: the first tuple
		:param tuple2: the second tuple
		:type tuple1: tuple
		:type tuple2: tuple
		:return: the multiplication-tuple
		:rtype: tuple of tuples
	"""
	res_list = list()
	for number in tuple1 :
		for number2 in tuple2 :
			res_list.append ((number, number2))
			res_list.append ((number2, number))
	return tuple(res_list)

first_tuple = (1, 2, 3)
second_tuple = (4, 5, 6)
print(mult_tuple(first_tuple, second_tuple))