res_list = list()


def sort_anagrams(list_of_strings):
	""" This function sorts a list of strings to their respective sublists (anagram lists)
		:param list_of_strings: the list of strings
		:type list_of_strings: list
		:return: the 'sorted' list
		:rtype: list
	"""
	# sort the elements
	for word in list_of_strings :
		assign_list (word, list_of_strings)
	return res_list	
		
def assign_list (str, list_str) :
	""" This function assign a sublist (anagram lists) for a given string
		:param str: the string 
		:param list_str: the list of sublists
		:type str: str
		:type list_str: list
		:rtype: None
	"""
	
	# check if the word's anagram exits in a list - it needs to be assigned to that list
	# otherwise, open a new list for it
	global res_list
	for lst in res_list :
		if sorted(str) == sorted (lst[0]) : # the correct list for this word
			lst.append(str)
			return 
	# didnt fint a correct list for it 
	res_list.append([str])
	
list_of_words = ['deltas', 'retainers', 'desalt', 'pants', 'slated', 'generating', 'ternaries', 'smelters', 'termless', 'salted', 'staled', 'greatening', 'lasted', 'resmelts']	
print(sort_anagrams(list_of_words))