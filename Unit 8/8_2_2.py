import functools
def sort_prices(list_of_tuples):
	"""
		: param list_of_tuples: a list of tuples cotaining an item and its price
		the tuples are in the format of ('item', 'price')
		:type list_of_tuples: a list of tuples
		:return: the sorted list, by the second element of the tuple, reversed
		:rytpe: a list of tuples
		
		I will use the sorted fucntion which returns a sorted list of the elements, and 
		recieves a list.
		1)	I also provided the 'sorted' fucntion with the requeried comparison function 
			with which to compare between each two tuple, and i set it to be the function cmp_tuples
			which orders it to look at the second element of the tuple i.e. the price (with a float casting)
		2)	In addition to that, i also asked the sorted function to sort the elements in a reversed order,
			as asked in the exercise.
	"""
	return sorted (list_of_tuples, key=cmp_tuples,reverse=True)
	
def cmp_tuples (tp1) : # comparison function
	return float(tp1[1])
	
# main	
products = [('milk', '5.5'), ('candy', '2.5'), ('bread', '9.0')]
print(sort_prices(products))